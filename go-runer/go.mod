module go-runner

go 1.15

require (
	github.com/owulveryck/onnx-go v0.5.0
	gorgonia.org/tensor v0.9.3
)
