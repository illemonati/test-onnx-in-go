package main

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/owulveryck/onnx-go"
	"github.com/owulveryck/onnx-go/backend/x/gorgonnx"
	"gorgonia.org/tensor"
)

const MODEL_PATH = "../python-model/xor.onnx"

var inputs = [][]float32{
	{0, 1},
	{1, 0},
	{0, 0},
	{1, 1},
}

func main() {
	backend := gorgonnx.NewGraph()
	model := onnx.NewModel(backend)
	b, _ := ioutil.ReadFile(MODEL_PATH)
	err := model.UnmarshalBinary(b)
	if err != nil {
		log.Fatal(err)
	}
	for _, input := range inputs {
		input_tensor := tensor.New(tensor.WithBacking(input), tensor.WithShape(1, 2))
		model.SetInput(0, input_tensor)
		err = backend.Run()
		output, _ := model.GetOutputTensors()
		fmt.Printf("%v : %v\n", input, output[0])
	}
}
