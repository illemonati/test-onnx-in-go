import torch
from torch import nn, optim, onnx
import torch.nn.functional as F
import numpy as np


class SimpleModelForXor(nn.Module):
    def __init__(self):
        super(SimpleModelForXor, self).__init__()
        self.fc0 = nn.Linear(2, 4)
        self.fc1 = nn.Linear(4, 3)
        self.fc2 = nn.Linear(3, 1)

    def forward(self, x):
        x = F.relu(self.fc0(x))
        x = F.relu(self.fc1(x))
        x = torch.sigmoid(self.fc2(x))
        return x


Xs = [
    [0, 0],
    [1, 0],
    [0, 1],
    [1, 1]
]

ys = [
    [0],
    [1],
    [1],
    [0]
]


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
Xs = torch.tensor(Xs, dtype=torch.float32).to(device)
ys = torch.tensor(ys, dtype=torch.float32).to(device)

print(Xs)
print(ys)

xor_model = SimpleModelForXor()
criterion = nn.BCELoss()
optimizer = optim.SGD(xor_model.parameters(), lr=0.01)
epochs = 3000

for epoch in range(epochs):
    corrects = 0
    total_loss = 0
    for X, y in zip(Xs, ys):
        optimizer.zero_grad()
        y_pred = xor_model(X)
        loss = criterion(y_pred, y)
        loss.backward()
        optimizer.step()
        total_loss += loss
        if torch.round(y_pred) == y:
            corrects += 1

    if epoch % 200 == 0:
        print(
            f'epoch {epoch+1}, average_loss {total_loss/len(Xs)}, average_acc {corrects/len(ys)}')


for X, y in zip(Xs, ys):
    y_pred = xor_model(X)
    print(f'X {X}, y {y}, y_pred {y_pred}')


onnx.export(
    xor_model,
    X,
    "xor.onnx",
    export_params=True,
    do_constant_folding=True,
    input_names=['input'],
    output_names=['output']
)
