import onnx
import onnxruntime

onnx_model = onnx.load("xor.onnx")
onnx.checker.check_model(onnx_model)

ort_session = onnxruntime.InferenceSession("xor.onnx")


# compute ONNX Runtime output prediction
ort_inputs = {ort_session.get_inputs()[0].name: [0, 1]}
ort_outs = ort_session.run(None, ort_inputs)

print(ort_outs)
