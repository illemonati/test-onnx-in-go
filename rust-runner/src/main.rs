use tract_ndarray::Array;
use tract_onnx::prelude::*;
use std::convert::TryInto;

const MODEL_PATH: &str = "../python-model/xor.onnx";

const INPUTS: [[f32; 2]; 4] = [
    [1.0, 0.0],
    [0.0, 1.0],
    [1.0, 1.0],
    [0.0, 0.0]
];

fn main() -> TractResult<()> {
    let model = tract_onnx::onnx()
    .model_for_path(MODEL_PATH)?
    .with_input_fact(0, InferenceFact::dt_shape(f32::datum_type(), tvec!(1, 2)))?
    .into_runnable()?;

    for input in &INPUTS{
        let input_tensor: Tensor = Array::from_shape_vec((1, 2), input.to_vec())?.try_into()?;
        println!("{:?}", input_tensor);
        let result = model.run(tvec!(input_tensor))?;
        println!("{:?} : {:?}", input, result)

    }

    Ok(())

}